package kr.forsee.rssiparser;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.io.IOException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.forsee.rssiparser.model.MacLayout;
import kr.forsee.rssiparser.model.Rssi;
import kr.forsee.rssiparser.service.BleService;
import kr.forsee.rssiparser.util.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Eungi on 2017-11-06.
 */
public class RssiParserFragment extends Fragment {
    private final String TAG = Constants.TAG + RssiParserFragment.class.getSimpleName();
    private static final int VISIBLE_X_RANGE_MAXIMUM = 3000;

    Messenger mBleService = null;
    final Messenger mMessenger = new Messenger(new RssiParserFragmentHandler());
    boolean mIsBleBound;

    class RssiParserFragmentHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BleService.MSG_SEND_RSSI:
                    //mRssiLineData = new LineData(Rssi.get().getLineDataSet());
                    mRssiLineData.notifyDataChanged();
                    mRssiLineChart.setData(mRssiLineData);
                    //mRssiLineChart.notifyDataSetChanged();
                    mRssiLineChart.setVisibleXRangeMaximum(VISIBLE_X_RANGE_MAXIMUM);
                    mRssiLineChart.moveViewToX(mRssiLineData.getEntryCount());
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private LineData mRssiLineData = new LineData();
    private List<MacLayout> mMacLayouts = new ArrayList<>();
    @BindView(R.id.rssiparser_input_file_name_edit_text) EditText mFileNameEditText;
    @BindView(R.id.rssiparser_input_file_name_button) Button mFileNameCheckButton;
    @BindView(R.id.rssiparser_log_explain_edit_text) EditText mExplainEditText;
    @BindView(R.id.rssiparser_dynamic_linear_layout) LinearLayout mDynamicLinearLayout;
    @BindView(R.id.rssiparser_dynamic_mac_edit_text) EditText mMacEditText;
    @BindView(R.id.rssiparser_dynamic_mac_button) Button mMacAddButton;
    @BindView(R.id.rssiparser_start_button) Button mStartButton;
    @BindView(R.id.rssiparser_show_rssi_line_chart) LineChart mRssiLineChart;

    public static RssiParserFragment newInstance() {
        return new RssiParserFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_rssiparser, container, false);
        ButterKnife.bind(this, v);

        mFileNameCheckButton.setOnClickListener(mOnClickListener);
        mMacAddButton.setOnClickListener(mOnClickListener);
        mStartButton.setOnClickListener(mOnClickListener);

        return v;
    }

    private View.OnClickListener mOnClickListener = v -> {
        switch (v.getId()) {
            case R.id.rssiparser_input_file_name_button:
                new CheckFileNameTask(getActivity()).execute(mFileNameEditText.getText().toString());
                break;
            case R.id.rssiparser_dynamic_mac_button:
                MacLayout macLayout = new MacLayout(getActivity(), mDynamicLinearLayout);
                macLayout.setClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        macLayout.removeView(mDynamicLinearLayout);
                        mMacLayouts.remove(macLayout);
                        Log.d(TAG, "onClick: macLayout size:" + mMacLayouts.size());
                    }
                });
                mMacLayouts.add(macLayout);
                break;
            case R.id.rssiparser_start_button:
                if ( !mIsBleBound) {
                    boolean isEmptyData = false;
                    if (mFileNameEditText.getText().toString().equals("") ||
                            mMacEditText.getText().toString().equals("") ||
                            mExplainEditText.getText().toString().equals("")) {
                        isEmptyData = true;
                    }
                    for (MacLayout aMacLayout : mMacLayouts) {
                        if (aMacLayout.getMac().equals("")) { isEmptyData = true; }
                    }
                    if (isEmptyData) {
                        Toast.makeText(getActivity(), "모든 데이터를 입력해주세요", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    doBindService();
                } else {
                    doUnbindService();
                }
                break;
        }
    };

    private void chartUpdate() {
        mRssiLineData = new LineData();
        LineDataSet[] lineDataSets = Rssi.get().getLineDataSets();
        for (LineDataSet aLineDataSet : lineDataSets) {
            mRssiLineData.addDataSet(aLineDataSet);
        }
        mRssiLineChart.clear();
        //mRssiLineChart.setData(mRssiLineData);
        mRssiLineChart.getAxisRight().setEnabled(false);
        mRssiLineChart.getLegend().setEnabled(false);
        mRssiLineChart.getAxisLeft().setDrawAxisLine(false);
        mRssiLineChart.getXAxis().setDrawAxisLine(false);
        //mRssiLineChart.getXAxis().setValueFormatter(formatter);
        mRssiLineChart.getDescription().setEnabled(false);
        mRssiLineChart.getAxisLeft().setGridColor(Color.rgb(220,220,220));
        mRssiLineChart.getXAxis().setGridColor(Color.rgb(220,220,220));
        mRssiLineChart.invalidate();
    }

    // mp android chart axis time 검색
    // https://github.com/PhilJay/MPAndroidChart/issues/789
    IAxisValueFormatter formatter = (value, axis) -> {
        String dateString = String.valueOf((int)value);
        Log.d(TAG, "formatter string date: " + dateString);
        return dateString.substring(0, 2) + "시" + dateString.substring(2, 4) + "분" + dateString.substring(4,8) + "초";
    };

    private String[] getMacArrFromEditTexts() {
        // 기본 mac 입력폼 + 추가 mac 입력폼 갯수
        int macSize = mMacLayouts.size() + 1;
        String[] macArr = new String[macSize];
        macArr[0] = mMacEditText.getText().toString();
        for (int i = 0; i < mMacLayouts.size(); i++) {
            macArr[i+1] = mMacLayouts.get(i).getMac();
        }
        return macArr;
    }

    private void rssiAndChartDataSetSettings(String[] macArr) {
        Rssi.get().releaseDataSet();
        Rssi.get().setDataSet(mMacLayouts.size() + 1, getResources().getStringArray(R.array.dataset_color_string_array), macArr);
    }

    void uiUpdate() {
        if (mIsBleBound) {
            mStartButton.setText("파싱 중지");
        } else {
            mStartButton.setText("파싱 시작");
        }
    }

    void doBindService() {
        getActivity().bindService(BleService.newIntent(getActivity()), mBleConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mBleConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "onBleServiceConnected");
            mIsBleBound = true;
            uiUpdate();
            mBleService = new Messenger(service);
            sendMessageToBleService(BleService.MSG_RECV_BIND);
            String[] macArr = getMacArrFromEditTexts();
            rssiAndChartDataSetSettings(macArr);
            chartUpdate();
            doStartService(macArr);
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mBleService = null;
            mIsBleBound = false;
        }
    };

    private void doStartService(String[] macArr) {
        Intent i = BleService.newIntent(getActivity(), mFileNameEditText.getText().toString(),
                macArr, mExplainEditText.getText().toString());
        getActivity().startService(i);
    }

    private void sendMessageToBleService(int msg_what) {
        if (mIsBleBound) {
            if (mBleService != null) {
                try {
                    Message msg = Message.obtain(null, msg_what, 0, 0);
                    msg.replyTo = mMessenger;
                    mBleService.send(msg);
                }
                catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        doUnbindService();
        Rssi.get().releaseDataSet();
        super.onDestroy();
    }

    void doUnbindService() {
        if (mIsBleBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mBleService != null) {
                sendMessageToBleService(BleService.MSG_RECV_UNBIND);
            }
            // Detach our existing connection.
            getActivity().unbindService(mBleConnection);
            mIsBleBound = false;
        }
        uiUpdate();
    }

}
