package kr.forsee.rssiparser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;

import kr.forsee.rssiparser.util.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *  Created by Eungi on 2017-11-06.
 */

public class CheckFileNameTask extends AsyncTask<String, Void, String> {
    private static final String TAG = Constants.TAG + CheckFileNameTask.class.getSimpleName();
    private Context mContext;
    private ProgressDialog mProgressDialog;

    public CheckFileNameTask(Context context) {
        mContext = context;
        mProgressDialog = new ProgressDialog(mContext);
    }


    @Override
    protected void onPreExecute() {
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("파일 이름 중복 체크중");
        mProgressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... param) {
        String fileName = param[0];
        try {
            String urlString = Uri.parse(Constants.WEB_URL.FILE_NAME).buildUpon()
                    .appendQueryParameter("fileName", fileName)
                    .build().toString();
            Log.d(TAG, "GetLocationService.onConnected().url : " + urlString);
            URL url = new URL(urlString);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        mProgressDialog.dismiss();
        if (result.equals("true")) {
            Toast.makeText(mContext, "이 파일 이름은 중복됨!", Toast.LENGTH_SHORT).show();
        } else if (result.equals("false")) {
            Toast.makeText(mContext, "이 파일 이름은 사용 가능", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "인터넷 미연결이나 파라미터 오류", Toast.LENGTH_SHORT).show();
        }
        super.onPostExecute(result);
    }
}