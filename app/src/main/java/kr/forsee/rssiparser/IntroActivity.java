package kr.forsee.rssiparser;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import kr.forsee.rssiparser.ble.BleWrapper;
import kr.forsee.rssiparser.ble.BleWrapperUiCallbacks;
import kr.forsee.rssiparser.util.Constants;

/**
 * Created by Eungi on 2017-11-06.
 */

public class IntroActivity extends AppCompatActivity {
    private static final String TAG = Constants.TAG + IntroActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 101;
    private static final int REQUEST_ENABLE_BLUETOOTH = 11;
    // 딜레이를 주기 위해 핸들러 사용
    // 시간이 지나면 메인액티비티로 이동할것이다.
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
        } else {
            systemIntro();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    systemIntro();
                } else {
                    Toast.makeText(this, "블루투스 LE 스캔을 위해선 위치 권한이 필요합니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    private void systemIntro() {
        BleWrapper mBleWrapper = new BleWrapper(getApplicationContext(), null);
        if ( !mBleWrapper.checkBleHardwareAvailable()) {
            Toast.makeText(this, "블루투스 LE 를 지원하지 않습니다.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if ( !mBleWrapper.isBtEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
        } else {
            mHandler = new Handler(); //딜레이를 주기 위해 핸들러 생성
            mHandler.postDelayed(gotoMain, 200); // 딜레이 ( 런어블 객체는 gotoMain, 시간 0.x초)
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_CANCELED) {
                // RESULT_CANCELED
                Toast.makeText(this, "블루투스를 켜 주세요", Toast.LENGTH_SHORT).show();
                finish();
                return;
            } else if (resultCode == Activity.RESULT_OK) {
                mHandler = new Handler(); //딜레이를 주기 위해 핸들러 생성
                mHandler.postDelayed(gotoMain, 200); // 딜레이 ( 런어블 객체는 gotoMain, 시간 0.x초)
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    Runnable gotoMain = () -> {
        startActivity(MainActivity.newIntent(getApplicationContext()));
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        // overridePendingTransition 이란 함수를 이용하여 fade in,out 효과를줌. 순서가 중요
    };

    // 인트로 중에 뒤로가기를 누를 경우 핸들러를 끊어버려 아무일 없게 만드는 부분
    // 미 설정시 인트로 중 뒤로가기를 누르면 인트로 후에 홈화면이 나옴.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacks(gotoMain);
    }

}
