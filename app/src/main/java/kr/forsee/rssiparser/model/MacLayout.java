package kr.forsee.rssiparser.model;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import kr.forsee.rssiparser.R;

/**
 * Created by Eungi on 2017-11-09.
 */

public class MacLayout {
    private LinearLayout mLinearLayout;
    private EditText mEditText;
    private Button mButton;

    public MacLayout(Activity activity, ViewGroup parent) {
        mLinearLayout = new LinearLayout(activity);
        mLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

        mEditText = new EditText(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = 1;
        mEditText.setLayoutParams(params);

        mButton = (Button) activity.getLayoutInflater().inflate(R.layout.button_custom, null);

        mLinearLayout.addView(mEditText);
        mLinearLayout.addView(mButton);
        parent.addView(mLinearLayout);
    }

    public void setClickListener(View.OnClickListener listener) {
        mButton.setOnClickListener(listener);
    }

    public void removeView(ViewGroup parent) {
        parent.removeView(mLinearLayout);
    }

    public String getMac() {
        return mEditText.getText().toString();
    }

}
