package kr.forsee.rssiparser.model;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static kr.forsee.rssiparser.util.Constants.TAG;

/**
 *  Created by Eungi on 2017-11-06.
 */

public class Rssi {
    private static final String RSSI_LEGENT = "RSSI_LEGENT";
    private static Rssi sRssi;
    private LineDataSet mLineDataSet;
    private LineDataSet[] mLineDataSets;
    private String[] mMacArr;

    public void setDataSet(int count, String[] colorStringArray, String[] macArr) {
        mLineDataSets = new LineDataSet[count];
        for (int i = 0; i < mLineDataSets.length; i++) {
            mLineDataSets[i] = new LineDataSet(new ArrayList<>(), RSSI_LEGENT + "#" + i);
            mLineDataSets[i].setColor(Color.parseColor(colorStringArray[i%colorStringArray.length]));
            //mLineDataSets[i].setColor(Color.parseColor("#881600"));
        }
        mMacArr = macArr;
    }
    public void releaseDataSet() {
        mLineDataSets = null;
        mMacArr = null;
    }

    private Rssi() {
        //mLineDataSet = new LineDataSet(new ArrayList<>(), RSSI_LEGENT);
        //mLineDataSet.addEntry(new Entry(0, 0));
        //mLineDataSet.setDrawCircles(false);
        //mLineDataSet.setDrawValues(false);
        //mLineDataSet.setColor(Color.rgb(136, 22, 0));
        //mLineDataSet.setColor(Color.parseColor("#881600"));
    }

    public static Rssi get() {
        if (sRssi == null) {
            sRssi = new Rssi();
        }
        return sRssi;
    }

    public LineDataSet getLineDataSet() {
        return mLineDataSet;
    }
    public LineDataSet[] getLineDataSets() {
        return mLineDataSets;
    }

    public void addRssi(int rssi) {
        mLineDataSet.addEntry(new Entry(mLineDataSet.getEntryCount(), rssi));
    }
    public boolean addRssi(String mac, int rssi, long time) {
        for (int i = 0; i < mMacArr.length; i++) {
            if (mMacArr[i].equals(mac)) {
                Date date = new Date(time);
                String timeStampStr = new SimpleDateFormat("HHmmssSS", Locale.US).format(date);
                int timeStamp = Integer.parseInt(timeStampStr);
                mLineDataSets[i].addEntry(new Entry(timeStamp, rssi));
                return true;
            }
        }
        return false;
    }

    public void clearRssi() {
        mLineDataSet = new LineDataSet(new ArrayList<>(), RSSI_LEGENT);
    }
}
