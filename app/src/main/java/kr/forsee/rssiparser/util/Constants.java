package kr.forsee.rssiparser.util;

/**
 * Created by Eungi on 2017-11-06.
 */

public class Constants {
    private Constants() {}

    public static final String TAG = "RSSI_PARSER_";

    public interface WEB_URL {
        String FILE_NAME = "http://ec2-13-124-68-131.ap-northeast-2.compute.amazonaws.com/SearchFile.php";
        String RSSI_LOG = "http://ec2-13-124-68-131.ap-northeast-2.compute.amazonaws.com/RssiParsing.php";
    }
}
