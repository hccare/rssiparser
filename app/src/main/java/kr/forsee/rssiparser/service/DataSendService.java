package kr.forsee.rssiparser.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import kr.forsee.rssiparser.util.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *  Created by Eungi on 2017-08-22.
 */

public class DataSendService extends IntentService {
    private static final String TAG = Constants.TAG + DataSendService.class.getSimpleName();
    private static final String NAME = TAG;
    private static final String EXTRA_FILE_NAME = "kr.forsee.rssiparser.EXTRA_FILE_NAME";
    private static final String EXTRA_MAC = "kr.forsee.rssiparser.EXTRA_MAC";
    private static final String EXTRA_RSSI = "kr.forsee.rssiparser.EXTRA_RSSI";
    private static final String EXTRA_TIME_STAMP = "kr.forsee.rssiparser.EXTRA_TIME_STAMP";
    private static final int DEFAULT_EXTRA_RSSI = -50;
    private static final long DEFAULT_EXTRA_TIME = 0L;

    public static Intent newIntent(Context context, String fileName, String mac, int rssi, long time) {
        Intent intent = new Intent(context, DataSendService.class);
        intent.putExtra(EXTRA_FILE_NAME, fileName);
        intent.putExtra(EXTRA_MAC, mac);
        intent.putExtra(EXTRA_RSSI, rssi);
        intent.putExtra(EXTRA_TIME_STAMP, time);
        return intent;
    }
    public static Intent newIntent(Context context, String fileName, String mac, int rssi, String time) {
        Intent intent = new Intent(context, DataSendService.class);
        intent.putExtra(EXTRA_FILE_NAME, fileName);
        intent.putExtra(EXTRA_MAC, mac);
        intent.putExtra(EXTRA_RSSI, rssi);
        intent.putExtra(EXTRA_TIME_STAMP, time);
        return intent;
    }
    public DataSendService() { super(NAME); }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "DataSendService onCreate");

    }

    // BleService 가 처음 startService 될 경우 mac 란에 explain 을 넣어서 보냅니다.
    // 그 이후 mac 란에는 mac 이 옵니다.
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "onHandleIntent");
        String mFileName = intent.getStringExtra(EXTRA_FILE_NAME);
        String mac = intent.getStringExtra(EXTRA_MAC);
        int rssi = intent.getIntExtra(EXTRA_RSSI, DEFAULT_EXTRA_RSSI);
        long time = intent.getLongExtra(EXTRA_TIME_STAMP, DEFAULT_EXTRA_TIME);
        Date date = new Date(time);
        String timeStamp = new SimpleDateFormat("MMddHHmmssSS", Locale.US).format(date);
        //String timeStamp = intent.getStringExtra(EXTRA_TIME_STAMP);
        try {
            String urlString = Uri.parse(Constants.WEB_URL.RSSI_LOG).buildUpon()
                    .appendQueryParameter("fileName", mFileName)
                    .appendQueryParameter("mac", mac)
                    .appendQueryParameter("rssi", String.valueOf(rssi))
                    .appendQueryParameter("timeStamp", timeStamp)
                    .build().toString();
            Log.d(TAG, "DataSendService.onConnected().url : " + urlString);
            URL url = new URL(urlString);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            Toast.makeText(this, "서버로 로그 전송에 실패했습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "DataSendService onDestroy");
    }

}
