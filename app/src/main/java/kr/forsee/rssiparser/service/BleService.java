package kr.forsee.rssiparser.service;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kr.forsee.rssiparser.ble.BleWrapper;
import kr.forsee.rssiparser.ble.BleWrapperUiCallbacks;
import kr.forsee.rssiparser.model.Rssi;
import kr.forsee.rssiparser.util.Constants;


/**
 *  Created by HAPPY on 2017-10-12. hi
 */

public class BleService extends Service {
    private final String TAG = Constants.TAG + BleService.class.getSimpleName();
    private static final String EXTRA_FILE_NAME = "kr.forsee.rssiparser.EXTRA_FILE_NAME";
    private static final String EXTRA_MAC = "kr.forsee.rssiparser.EXTRA_MAC";
    private static final String EXTRA_EXPLAIN = "kr.forsee.rssiparser.EXTRA_EXPLAIN";
    public static final String MSG_SEND_STRING_KEY = "kr.forsee.rssiparser.MSG_SEND_STRING_KEY";
    public static final int MSG_RECV_BIND = 0;
    public static final int MSG_RECV_UNBIND = 1;
    public static final int MSG_SEND_STRING = 2;
    public static final int MSG_SEND_RSSI = 3;

    List<Messenger> mClients = new ArrayList<>(); // Keeps track of all current registered clients.
    //int mValue = 0; // Holds last value set by a client.
    final Messenger mMessenger = new Messenger(new BleServiceHandler()); // Target we publish for clients to send messages to IncomingHandler.
    class BleServiceHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_RECV_BIND:
                    Log.d(TAG, "handleMessage MSG_RECV_BIND");
                    mClients.add(msg.replyTo);
                    break;
                case MSG_RECV_UNBIND:
                    Log.d(TAG, "handleMessage MSG_RECV_UNBIND");
                    mClients.remove(msg.replyTo);
                    stopSelf();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private BleWrapper mBleWrapper = null;
    private String mFileName;
    private String[] mMacArr;
    private String mExplain;

    public static Intent newIntent(Context context) {
        return new Intent(context, BleService.class);
    }

    public static Intent newIntent(Context context, String fileName, String[] mac, String explain) {
        Intent intent = new Intent(context, BleService.class);
        intent.putExtra(EXTRA_FILE_NAME, fileName);
        intent.putExtra(EXTRA_MAC, mac);
        intent.putExtra(EXTRA_EXPLAIN, explain);
        return intent;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // create BleWrapper with empty callback object except uiDeficeFound function (we need only that here)
        mBleWrapper = new BleWrapper(getApplicationContext(), new BleWrapperUiCallbacks.Null() {
            @Override
            public void uiDeviceFound(final BluetoothDevice device, final int rssi, final byte[] record) {
                handleFoundDevice(device, rssi, record);
            }
        });

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        mFileName = intent.getStringExtra(EXTRA_FILE_NAME);
        mMacArr = intent.getStringArrayExtra(EXTRA_MAC);
        mExplain = intent.getStringExtra(EXTRA_EXPLAIN);
        startService(DataSendService.newIntent(getApplicationContext(), mFileName, mExplain, 1, new Date().getTime()));

        // initialize BleWrapper object
        mBleWrapper.initialize();
        // Automatically start scanning for devices
        // remember to add timeout for scanning to not run it forever and drain the battery
        //addScanningTimeout();
        mBleWrapper.startScanning();


        return START_STICKY;
    }

    /* add device to the current list of devices */
    private void handleFoundDevice(final BluetoothDevice device,
                                   final int rssi,
                                   final byte[] scanRecord) {
        //if (checkMac(device.getAddress())) {
        //if (checkMac("62:E2:F6:E3:2F:2C")) {
        long time = new Date().getTime();
        boolean checkMac = Rssi.get().addRssi(device.getAddress(), rssi, time);
        if (checkMac) {
            sendMessageToUI(MSG_SEND_RSSI);
            //Toast.makeText(getApplicationContext(), "받음! Add: " + device.getAddress() + ", rssi: " + rssi , Toast.LENGTH_SHORT).show();
            startService(DataSendService.newIntent(getApplicationContext(), mFileName, device.getAddress(), rssi, time));
        }
        //}
    }

    private boolean checkMac(String mac) {
        for (int i = 0; i < mMacArr.length; i++) {
            if (mMacArr[i].equals(mac)) {
                return true;
            }
        }
        return false;
    }


    // 이 외 여러가지 함수 오버라이딩 해서 사용, 갯수 많아지면 헬퍼클래스 작성
    private void sendMessageToUI(int msg_what) {
        for (int i=mClients.size()-1; i>=0; i--) {
            try {
                mClients.get(i).send(Message.obtain(null, msg_what));
            }
            catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }
    private void sendMessageToUI(String msg_str) {
        for (int i=mClients.size()-1; i>=0; i--) {
            try {
                //Send data as a String
                Bundle b = new Bundle();
                b.putString(MSG_SEND_STRING_KEY, msg_str);
                Message msg = Message.obtain(null, MSG_SEND_STRING);
                msg.setData(b);
                mClients.get(i).send(msg);
            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "on Unbind");
        mBleWrapper.stopScanning();
        mMacArr = null;
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }
}
